FROM frolvlad/alpine-oraclejdk8:slim
VOLUME /tmp
ADD target/skydiving4sell-0.0.1-SNAPSHOT.jar app.jar
RUN apk add --no-cache openssl

ENV DOCKERIZE_VERSION v0.6.1
RUN wget https://github.com/jwilder/dockerize/releases/download/$DOCKERIZE_VERSION/dockerize-alpine-linux-amd64-$DOCKERIZE_VERSION.tar.gz \
    && tar -C /usr/local/bin -xzvf dockerize-alpine-linux-amd64-$DOCKERIZE_VERSION.tar.gz \
    && rm dockerize-alpine-linux-amd64-$DOCKERIZE_VERSION.tar.gz
ENV JAVA_OPTS=""
RUN sh -c 'touch /app.jar'
ENTRYPOINT ["sh", "-c" ,"java $JAVA_OPTS -Djava.security.egd=file:/dev/./urandom -jar /app.jar"]
