CREATE TABLE `PRODUCT` (
  `ID` int(11) NOT NULL,
  `TITLE` varchar(200) CHARACTER SET utf8 NOT NULL,
  `STATE` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `COUNTRY` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `CURRENCY` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `DESCRIPTION` longtext CHARACTER SET utf8,
  `PRICE` decimal(10,0) NOT NULL,
  `DAMAGES` bit(1) DEFAULT NULL,
  `IMAGES` varchar(45) CHARACTER SET utf8 DEFAULT NULL,
  `CREATED_DATE` timestamp NOT NULL,
  `LAST_UPDATED_DATE` timestamp NOT NULL,
  `VERSION` int(11) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `index2` (`TITLE`,`STATE`,`COUNTRY`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
