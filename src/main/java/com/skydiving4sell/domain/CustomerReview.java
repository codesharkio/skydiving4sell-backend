package com.skydiving4sell.domain;

import lombok.*;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "customer_review")
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@EqualsAndHashCode(callSuper = false)
public class CustomerReview extends BaseEntity<String> {

    @ManyToOne
    @JoinColumn(name = "fk_person")
    private Person person;
    /**
     * score to calculate customer review star
     */
    private int score;
    private String title;
    private String detail;

    @OneToMany
    private List<Image> reviewImages;

    @ManyToOne
    @JoinColumn(name = "fk_product")
    private Product product;


    //** custom code after this line **//
}
