package com.skydiving4sell.domain;

public enum PersonType {
    SELLER, BUYER
}
