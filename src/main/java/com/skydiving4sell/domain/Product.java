package com.skydiving4sell.domain;

import lombok.*;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by hungbang on 04/12/2018
 */
@Entity
@Table(name = "product")
@org.hibernate.annotations.Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@EqualsAndHashCode(callSuper = false)
public class Product extends BaseEntity<String> {
    private String title;
    private String titleSummary;
    private String modelNumber;
    private BigDecimal price;
    private String description;
    private String sizeNumber;
    private String color;
    /**
     * status of item when listing on the app
     */
    private boolean active;
    /**
     * listed date
     */
    private LocalDate listedDate;
    private String domesticShipping;
    /**
     * international shipping
     */
    private String internationalShipping;
    /**
     * product condition new or used
     */
    private ProductState productState;

    @OneToMany
    private List<Image> productImages = new ArrayList<>();

    @OneToOne
    @JoinColumn(name = "fk_manufacturer")
    private Manufacturer manufacturer;

    @OneToMany(mappedBy = "product")
    private List<CustomerReview> customerReviews = new ArrayList<>();

    //** custom code after this line **//
}
