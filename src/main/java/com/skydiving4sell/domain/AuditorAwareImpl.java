package com.skydiving4sell.domain;

import org.springframework.data.domain.AuditorAware;

import java.util.Optional;

/**
 * Created by hungbang on 05/12/2018
 */
public class AuditorAwareImpl implements AuditorAware<String> {
    @Override
    public Optional<String> getCurrentAuditor() {
        // TODO: [HBQ] return the current user is editing of the application
        return Optional.empty();
    }
}
