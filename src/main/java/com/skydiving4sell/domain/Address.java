package com.skydiving4sell.domain;

import lombok.*;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "address")
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@EqualsAndHashCode(callSuper = false)
public class Address extends BaseEntity<String> {
    private String address1;
    private String address2;
    private String district;
    private String city;
    private String country;


    //** custom code after this line **//
}
