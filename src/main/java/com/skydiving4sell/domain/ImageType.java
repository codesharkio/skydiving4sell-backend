package com.skydiving4sell.domain;

public enum ImageType {
    PRODUCT,
    REVIEW,
    PROFILE
}
