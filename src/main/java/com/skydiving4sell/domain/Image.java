package com.skydiving4sell.domain;


import lombok.*;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "image")
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@EqualsAndHashCode(callSuper = false)
public class Image extends BaseEntity<String> {
    private ImageType imageType;
    private String url;
    private String data;
    private String name;
    private String extension;
    private long size;
}
