package com.skydiving4sell.domain;

import lombok.*;

import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "person")
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@EqualsAndHashCode(callSuper = false)
public class Person extends BaseEntity<String> {
    private String firstName;
    private String lastName;
    private Gender gender;

    @OneToMany
    private List<Image> profiles = new ArrayList<>();
    private LocalDate birthday;
    @OneToMany
    private List<Address> addresses = new ArrayList<>();
    private PersonType personType;
    private String phoneNumber;
    private String email;
    @OneToMany(mappedBy = "person")
    private List<CustomerReview> customerReviews = new ArrayList<>();

    //** custom code after this line **//
}
