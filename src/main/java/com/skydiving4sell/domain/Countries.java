package com.skydiving4sell.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * Created by hungbang on 05/12/2018
 */
@Data
@EqualsAndHashCode(callSuper = false)
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "countries")
@org.hibernate.annotations.Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Countries extends BaseEntity<String> {
    private String name;
    private String alpha2Code;
    private String alpha3Code;
    private String numericCode;
    private String nativeName;
    private String currencyCode;
    private String flag;

    //** custom code after this line **//
}
