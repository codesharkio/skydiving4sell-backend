package com.skydiving4sell.domain;

import lombok.*;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "manufacturer")
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@EqualsAndHashCode(callSuper = false)
public class Manufacturer extends BaseEntity<String> {
    private String name;



    //** custom code after this line **//
}
