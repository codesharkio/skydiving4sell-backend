package com.skydiving4sell.config;

import com.atomikos.icatch.jta.UserTransactionImp;
import com.atomikos.jdbc.AtomikosDataSourceBean;
import com.google.common.collect.Maps;
import com.hazelcast.hibernate.HazelcastCacheRegionFactory;
import com.mysql.cj.jdbc.MysqlXADataSource;
import com.skydiving4sell.properties.DataSourceProperties;
import org.hibernate.cfg.AvailableSettings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.*;
import org.springframework.orm.jpa.JpaVendorAdapter;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;

import javax.sql.DataSource;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;
import java.sql.SQLException;
import java.util.HashMap;

/**
 * Created by hungbang on 04/12/2018
 */
@Configuration
@EnableAspectJAutoProxy(proxyTargetClass = true)
public class DataSourceConfig {

    private final DataSourceProperties dataSourceProperties;


    private final JpaVendorAdapter jpaVendorAdapter;

    @Autowired
    public DataSourceConfig(DataSourceProperties dataSourceProperties, JpaVendorAdapter jpaVendorAdapter) {
        this.dataSourceProperties = dataSourceProperties;
        this.jpaVendorAdapter = jpaVendorAdapter;
    }


    @Primary
    @Bean
    public DataSource dataSource() throws SQLException {
        MysqlXADataSource mysqlXADataSource = new MysqlXADataSource();
        mysqlXADataSource.setUrl(dataSourceProperties.getUrl());
        mysqlXADataSource.setPinGlobalTxToPhysicalConnection(true);
        mysqlXADataSource.setUser(dataSourceProperties.getUsername());
        mysqlXADataSource.setPassword(dataSourceProperties.getPassword());

        AtomikosDataSourceBean atomikosDataSourceBean = new AtomikosDataSourceBean();
        atomikosDataSourceBean.setXaDataSource(mysqlXADataSource);
        atomikosDataSourceBean.setUniqueResourceName("xads1");
        atomikosDataSourceBean.setTestQuery(dataSourceProperties.getAtomikos().getTestQuery());
        return atomikosDataSourceBean;
    }


    /** Also use Atomikos UserTransactionImp, needed to configure Spring. */
    @Bean
    public UserTransaction atomikosUserTransaction() throws SystemException {
        final UserTransactionImp result = new UserTransactionImp();
        result.setTransactionTimeout(dataSourceProperties.getAtomikos().getTransactionTimeout());
        return result;
    }

    @Primary
    @Bean("entityManagerFactory")
    @DependsOn("transactionManager")
    public LocalContainerEntityManagerFactoryBean entityManagerFactoryBean() throws SQLException {
        HashMap<String,Object> properties = Maps.newHashMap();
        properties.put("hibernate.transaction.jta.platform", AtomikosJtaPlatform.class.getName());
        properties.put("javax.persistence.transactionType", "JTA");
        properties.put(org.hibernate.cfg.AvailableSettings.USE_SECOND_LEVEL_CACHE,  dataSourceProperties.getHibernate().getCache().isUseSecondLevelCache());
        properties.put(AvailableSettings.USE_QUERY_CACHE,  dataSourceProperties.getHibernate().getCache().isUseQueryCache());
        properties.put(AvailableSettings.CACHE_REGION_FACTORY, HazelcastCacheRegionFactory.class);
        // TODO: [HBQ] will be moving this config value to external config
        properties.put("hibernate.cache.hazelcast.use_native_client", true);
        properties.put("hibernate.cache.hazelcast.native_client_address", "hazelcast_service:5701");
        properties.put("hibernate.cache.hazelcast.native_client_group", "skydiving4sell-hazelcast-group");
        properties.put("hibernate.cache.hazelcast.native_client_password", "password");

        LocalContainerEntityManagerFactoryBean em = new LocalContainerEntityManagerFactoryBean();
        em.setJtaDataSource(dataSource());
        em.setJpaVendorAdapter(jpaVendorAdapter);
        em.setPackagesToScan("com.skydiving4sell.domain");
        em.setPersistenceUnitName("skydivingPersistence");
        em.setJpaPropertyMap(properties);
        return em;
    }


}
