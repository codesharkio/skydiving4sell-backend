package com.skydiving4sell.mapper;

import com.fasterxml.jackson.databind.module.SimpleModule;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateDeserializer;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateSerializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalTimeSerializer;
import com.skydiving4sell.utils.DateUtils;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

/**
 * Created by hungbang on 04/12/2018
 */
public class DateTimeJacksonModule extends SimpleModule {

    public DateTimeJacksonModule() {
        DateTimeFormatter datePattern = DateTimeFormatter.ofPattern(DateUtils.DATE_PATTERN);
        DateTimeFormatter dateTimePattern = DateTimeFormatter.ofPattern(DateUtils.DATE_TIME_PATTERN);
        DateTimeFormatter timePattern = DateTimeFormatter.ofPattern(DateUtils.TIME_PATTERN);

        addSerializer(LocalDate.class, new LocalDateSerializer(datePattern));
        addSerializer(LocalDateTime.class, new LocalDateTimeSerializer(dateTimePattern));
        addSerializer(LocalTime.class, new LocalTimeSerializer(timePattern));

        addDeserializer(LocalDate.class, new LocalDateDeserializer(datePattern));
        addDeserializer(LocalDateTime.class, new LocalDateTimeDeserializer(dateTimePattern));
        addDeserializer(LocalTime.class, new LocalTimeDeserializer(timePattern));
    }
}
