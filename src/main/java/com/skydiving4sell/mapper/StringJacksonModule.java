package com.skydiving4sell.mapper;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.module.SimpleModule;

import java.io.IOException;

/**
 * Auto trim to null String value from JSON data object
 * <p>
 * Created by hungbang on 04/12/2018
 */
public class StringJacksonModule extends SimpleModule {

    public StringJacksonModule() {
        addDeserializer(String.class, new JsonDeserializer<String>() {
            @Override
            public String deserialize(JsonParser jp, DeserializationContext ctxt)
                    throws IOException {
                if (jp.getValueAsString().trim().length() == 0) {
                    return null;
                }
                return jp.getValueAsString();
            }
        });
    }
}
