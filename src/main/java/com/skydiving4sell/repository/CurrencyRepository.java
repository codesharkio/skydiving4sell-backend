package com.skydiving4sell.repository;

import com.skydiving4sell.domain.Currency;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.List;

/**
 * Created by hungbang on 05/12/2018
 */
public interface CurrencyRepository extends JpaRepository<Currency, String>, JpaSpecificationExecutor<Currency> {

    @Cacheable("findAllByCode")
    List<Currency> findAllByCode(final String code);
}
