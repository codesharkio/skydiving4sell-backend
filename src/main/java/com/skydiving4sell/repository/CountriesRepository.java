package com.skydiving4sell.repository;

import com.skydiving4sell.domain.Countries;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.List;

/**
 * Created by hungbang on 05/12/2018
 */
public interface CountriesRepository extends JpaRepository<Countries, String>, JpaSpecificationExecutor<Countries> {

    @Cacheable("findByNameContains")
    List<Countries> findAllByNameContains(final String name);

}
