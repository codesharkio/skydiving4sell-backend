package com.skydiving4sell.repository;

import com.skydiving4sell.domain.CustomerReview;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface CustomerReviewRepository extends JpaRepository<CustomerReview, String>, JpaSpecificationExecutor<CustomerReview> {

}
