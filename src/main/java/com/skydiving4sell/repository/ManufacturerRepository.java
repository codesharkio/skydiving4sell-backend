package com.skydiving4sell.repository;

import com.skydiving4sell.domain.Manufacturer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface ManufacturerRepository extends JpaRepository<Manufacturer, String>, JpaSpecificationExecutor<Manufacturer> {
}
