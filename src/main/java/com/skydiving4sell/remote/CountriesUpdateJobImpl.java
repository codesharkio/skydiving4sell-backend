package com.skydiving4sell.remote;

import com.github.dozermapper.core.Mapper;
import com.skydiving4sell.domain.Countries;
import com.skydiving4sell.domain.Currency;
import com.skydiving4sell.model.CountriesDto;
import com.skydiving4sell.model.CurrencyDto;
import com.skydiving4sell.service.CountriesService;
import com.skydiving4sell.service.CurrencyService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.retry.annotation.Backoff;
import org.springframework.retry.annotation.Retryable;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by hungbang on 05/12/2018
 */
@Component
public class CountriesUpdateJobImpl implements CountriesUpdateJob {

    private final Logger log = LoggerFactory.getLogger(CountriesUpdateJobImpl.class);

    private final CountriesResource countriesResource;

    private final CountriesService countriesService;

    private final Mapper dozerMapper;

    private final CurrencyService currencyService;

    @Autowired
    public CountriesUpdateJobImpl(CountriesResource countriesResource, CountriesService countriesService,
                                  Mapper dozerMapper, CurrencyService currencyService) {
        this.countriesResource = countriesResource;
        this.countriesService = countriesService;
        this.dozerMapper = dozerMapper;
        this.currencyService = currencyService;
    }

    @Override
    @Retryable(value = {Exception.class}, backoff = @Backoff(delay = 1000 * 60))
    @Scheduled(cron = "0 0/30 * * * *") // Every 30 minutes
    @EventListener(ApplicationReadyEvent.class)
    public void update() {
        long t0 = System.currentTimeMillis();

        if (countriesService.checkExistingCountries()) {
            return;
        }
        log.info("Scheduled Job: Scheduled countries update has been started");
        log.info("Scheduled Job: Downloading sources...");
        try {
            final List<CountriesDto> countriesDtos = countriesResource.getCountries();

            log.info("Downloaded list Country, timestamp: {}, size: {}",
                    System.currentTimeMillis(), countriesDtos.size());
            List<Countries> countries = countriesDtos.stream().map(countriesDto -> {
                Countries coun = dozerMapper.map(countriesDto, Countries.class);
                CurrencyDto currencyDto = countriesDto.getCurrencies().stream().findFirst().orElse(null);
                coun.setCurrencyCode(currencyDto != null ? currencyDto.getCode() : null);
                return coun;
            }).collect(Collectors.toList());

            List<Currency> currencies = countriesDtos.stream().map(countriesDto -> {
                CurrencyDto currencyDto = countriesDto.getCurrencies().stream().findFirst().orElse(null);
                return dozerMapper.map(currencyDto, Currency.class);
            }).collect(Collectors.toList());

            currencyService.saveCurrencies(currencies);
            log.info("Creating new currency list");

            countriesService.saveCountries(countries);
            log.info("Creating new countries list");
        } catch (Exception ex) {
            log.warn("Countries update has been failed with: {}", ex.getMessage());
            log.warn("Restarting due to update failure...");
            throw ex;
        }
        long t1 = System.currentTimeMillis();
        log.info("Scheduled Job: Scheduled sanctions update has been finished in {} seconds", (t1 - t0) / 1000);
    }
}
