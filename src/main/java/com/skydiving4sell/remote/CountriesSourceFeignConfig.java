package com.skydiving4sell.remote;

import feign.Feign;
import feign.Logger;
import feign.gson.GsonDecoder;
import feign.okhttp.OkHttpClient;
import feign.slf4j.Slf4jLogger;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Created by hungbang on 05/12/2018
 */
@Configuration
public class CountriesSourceFeignConfig {

    @Bean
    public CountriesResource getCountriesResource() {
        return Feign.builder()
                .client(new OkHttpClient())
                .decoder(new GsonDecoder())
                .logger(new Slf4jLogger(CountriesResource.class))
                .logLevel(Logger.Level.FULL)
                .target(CountriesResource.class, "https://");
    }

}
