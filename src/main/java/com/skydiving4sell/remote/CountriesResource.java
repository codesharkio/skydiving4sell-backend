package com.skydiving4sell.remote;

import com.skydiving4sell.model.CountriesDto;
import feign.Headers;
import feign.RequestLine;

import java.util.List;

/**
 * Created by hungbang on 05/12/2018
 */
@Headers("Content-Type: application/json")
public interface CountriesResource {

    String COUNTRIES_SOURCE = "restcountries.eu/rest/v2/all";

    @RequestLine("GET " + COUNTRIES_SOURCE)
    List<CountriesDto> getCountries();
}
