package com.skydiving4sell.service.impl;

import com.skydiving4sell.criterion.CurrenciesCriteria;
import com.skydiving4sell.domain.Currency;
import com.skydiving4sell.repository.CurrencyRepository;
import com.skydiving4sell.service.CurrencyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by hungbang on 05/12/2018
 */
@Service
@Transactional(rollbackFor = {Throwable.class})
public class CurrencyServiceImpl implements CurrencyService {

    @Autowired
    private CurrencyRepository currencyRepository;
    @Override
    public void saveCurrencies(List<Currency> currencies) {
        currencyRepository.saveAll(currencies);
    }

    @Override
    public List<Currency> findAllByCriteria(CurrenciesCriteria currenciesCriteria) {
        return currencyRepository.findAllByCode(currenciesCriteria.getCode());
    }

    @Override
    @Cacheable("findAll")
    public List<Currency> findAll() {
        return currencyRepository.findAll();
    }
}
