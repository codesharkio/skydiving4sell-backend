package com.skydiving4sell.service.impl;

import com.github.dozermapper.core.Mapper;
import com.skydiving4sell.domain.CustomerReview;
import com.skydiving4sell.domain.Image;
import com.skydiving4sell.domain.Manufacturer;
import com.skydiving4sell.domain.Product;
import com.skydiving4sell.exception.S4sEntityNotFoundException;
import com.skydiving4sell.model.ProductDto;
import com.skydiving4sell.repository.CustomerReviewRepository;
import com.skydiving4sell.repository.ImageRepository;
import com.skydiving4sell.repository.ManufacturerRepository;
import com.skydiving4sell.repository.ProductRepository;
import com.skydiving4sell.service.ProductService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Created by hungbang on 04/12/2018
 */
@Service
@Transactional(rollbackFor = {Throwable.class})
public class ProductServiceImpl implements ProductService {
    private static final Logger LOGGER = LoggerFactory.getLogger(ProductServiceImpl.class);
    private final ProductRepository productRepository;
    private final Mapper dozerMapper;
    private final CustomerReviewRepository customerReviewRepository;
    private final ManufacturerRepository manufacturerRepository;
    private final ImageRepository imageRepository;

    @Autowired
    public ProductServiceImpl(final ProductRepository productRepository,
                              final Mapper dozerMapper,
                              final CustomerReviewRepository customerReviewRepository,
                              final ManufacturerRepository manufacturerRepository,
                              final ImageRepository imageRepository) {
        this.customerReviewRepository = customerReviewRepository;
        this.productRepository = productRepository;
        this.dozerMapper = dozerMapper;
        this.manufacturerRepository = manufacturerRepository;
        this.imageRepository = imageRepository;
    }

    @Override
    public Product createProduct(ProductDto productDto) {
        Product product = dozerMapper.map(productDto, Product.class);
        CustomerReview customerReview = customerReviewRepository.save(dozerMapper.map(productDto.getCustomerReviews(), CustomerReview.class));
        product.setCustomerReviews(Collections.singletonList(customerReview));
        Manufacturer manufacturer = manufacturerRepository.save(dozerMapper.map(productDto.getManufacturer(), Manufacturer.class));
        product.setManufacturer(manufacturer);
        List<Image> images = productDto.getProductImages().stream().map(imageDto -> dozerMapper.map(imageDto, Image.class)).collect(Collectors.toList());
        product.setProductImages(images);
        return productRepository.save(product);
    }

    @Override
    public List<Product> findAll() {
        return productRepository.findAll();
    }

    @Override
    public Product findById(final String id) throws S4sEntityNotFoundException {
        final Optional<Product> optionalProduct = productRepository.findById(id);
        return optionalProduct.orElseThrow(() -> {
            LOGGER.error("Entity not found for id {}", id);
            return new S4sEntityNotFoundException("Entity not found for id :" + id);
        });
    }

    // TODO: can not update entity at the moment. Waiting for fix
    @Override
    public Product updateProduct(Product product) throws S4sEntityNotFoundException {
        Optional<Product> optionalProduct = productRepository.findById(product.getId());
        Product savedProduct = optionalProduct.orElseThrow(S4sEntityNotFoundException::new);
        BeanUtils.copyProperties(product, savedProduct);
        return this.productRepository.save(savedProduct);
    }
}
