package com.skydiving4sell.service.impl;

import com.github.dozermapper.core.Mapper;
import com.skydiving4sell.domain.Image;
import com.skydiving4sell.model.ImageDto;
import com.skydiving4sell.repository.ImageRepository;
import com.skydiving4sell.service.ImageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


@Service
@Transactional(rollbackFor = {Throwable.class})
public class ImageServiceImpl implements ImageService {

    private final ImageRepository imageRepository;
    private final Mapper dozerMapper;

    @Autowired
    public ImageServiceImpl(ImageRepository imageRepository, Mapper dozerBeanMapper) {
        this.imageRepository = imageRepository;
        this.dozerMapper = dozerBeanMapper;
    }


    @Override
    public Image storeImage(ImageDto imageDto) {
        return this.imageRepository.save(dozerMapper.map(imageDto, Image.class));
    }
}
