package com.skydiving4sell.service.impl;

import com.skydiving4sell.criterion.CountriesCriteria;
import com.skydiving4sell.domain.Countries;
import com.skydiving4sell.repository.CountriesRepository;
import com.skydiving4sell.service.CountriesService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by hungbang on 05/12/2018
 */
@Service
@Transactional(rollbackFor = {Throwable.class})
public class CountriesServiceImpl implements CountriesService {

    private final CountriesRepository countriesRepository;

    public CountriesServiceImpl(final CountriesRepository countriesRepository){
        this.countriesRepository = countriesRepository;
    }

    @Override
    public List<Countries> findAllByCriteria(CountriesCriteria countriesCriteria) {
        return countriesRepository.findAllByNameContains(countriesCriteria.getName());
    }

    @Override
    public void saveCountries(List<Countries> countries) {
        countriesRepository.saveAll(countries);
    }

    @Override
    public boolean checkExistingCountries() {
        return !countriesRepository.findAll().isEmpty();
    }
}
