package com.skydiving4sell.service;

import com.skydiving4sell.criterion.CurrenciesCriteria;
import com.skydiving4sell.domain.Currency;

import java.util.List;

/**
 * Created by hungbang on 05/12/2018
 */
public interface CurrencyService {

    void saveCurrencies(List<Currency> currencies);

    List<Currency> findAllByCriteria(CurrenciesCriteria currenciesCriteria);

    List<Currency> findAll();
}
