package com.skydiving4sell.service;

import com.skydiving4sell.domain.Product;
import com.skydiving4sell.exception.S4sEntityNotFoundException;
import com.skydiving4sell.model.ProductDto;

import java.util.List;

/**
 * Created by hungbang on 04/12/2018
 */
public interface ProductService {

    Product createProduct(ProductDto productDto);

    List<Product> findAll();

    Product findById(String id) throws S4sEntityNotFoundException;

    Product updateProduct(Product product) throws S4sEntityNotFoundException;
}
