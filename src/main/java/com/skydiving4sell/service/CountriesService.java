package com.skydiving4sell.service;

import com.skydiving4sell.criterion.CountriesCriteria;
import com.skydiving4sell.domain.Countries;
import org.springframework.http.ResponseEntity;

import java.util.List;

/**
 * Created by hungbang on 05/12/2018
 */
public interface CountriesService {
    List<Countries> findAllByCriteria(CountriesCriteria countriesCriteria);
    void saveCountries(List<Countries> countries);
    boolean checkExistingCountries();
}
