package com.skydiving4sell.service;

import com.skydiving4sell.domain.Image;
import com.skydiving4sell.model.ImageDto;

public interface ImageService {

    Image storeImage(ImageDto imageDto);

}
