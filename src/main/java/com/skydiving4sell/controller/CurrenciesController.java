package com.skydiving4sell.controller;

import com.skydiving4sell.criterion.CurrenciesCriteria;
import com.skydiving4sell.domain.Currency;
import com.skydiving4sell.service.CurrencyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Created by hungbang on 05/12/2018
 */
@RestController
@RequestMapping("api/v1/currencies")
public class CurrenciesController {

    @Autowired
    private CurrencyService currencyService;

    @GetMapping
    public ResponseEntity<List<Currency>> findAllByCriteria(final CurrenciesCriteria currenciesCriteria) {
        return ResponseEntity.ok(currencyService.findAllByCriteria(currenciesCriteria));
    }

    @GetMapping("/all")
    public ResponseEntity<List<Currency>> findAll() {
        return ResponseEntity.ok(currencyService.findAll());
    }

}
