package com.skydiving4sell.controller;


import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

@RestController
@RequestMapping("api/v1/uploader")
public class UploadController {

    // TODO: Considering either uploading the image to separate server or same server
    @PostMapping("/images/add")
    public ResponseEntity uploadImage(@RequestParam MultipartFile file, @RequestParam String imageType){
        return ResponseEntity.status(HttpStatus.CREATED).build();
    }

}
