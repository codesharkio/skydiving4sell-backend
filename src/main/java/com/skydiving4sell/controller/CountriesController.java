package com.skydiving4sell.controller;

import com.skydiving4sell.criterion.CountriesCriteria;
import com.skydiving4sell.domain.Countries;
import com.skydiving4sell.service.CountriesService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Created by hungbang on 05/12/2018
 */
@RestController
@RequestMapping("api/v1/countries")
public class CountriesController {

    private final CountriesService countriesService;

    public CountriesController(final CountriesService countriesService){
        this.countriesService = countriesService;
    }

    @GetMapping
    public ResponseEntity<List<Countries>> findAllByCriteria(final CountriesCriteria countriesCriteria){

        return ResponseEntity.ok(countriesService.findAllByCriteria(countriesCriteria));
    }

}
