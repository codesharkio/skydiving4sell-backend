package com.skydiving4sell.controller;

import com.github.dozermapper.core.Mapper;
import com.skydiving4sell.domain.Product;
import com.skydiving4sell.exception.S4sEntityNotFoundException;
import com.skydiving4sell.model.ProductDto;
import com.skydiving4sell.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by hungbang on 04/12/2018
 */
@RestController
@RequestMapping("api/v1/products")
public class ProductController {

    private final ProductService productService;
    private final Mapper dozerMapper;

    @Autowired
    public ProductController(ProductService productService, final Mapper dozerMapper) {
        this.productService = productService;
        this.dozerMapper = dozerMapper;
    }

    @GetMapping
    public ResponseEntity<List<ProductDto>> findAll() {
        return ResponseEntity.ok(productService.findAll().stream().map(product -> dozerMapper.map(product, ProductDto.class))
                .collect(Collectors.toList()));
    }

    @GetMapping("/{id}")
    public ResponseEntity<Product> findById(@PathVariable("id") String id) throws S4sEntityNotFoundException {
        return ResponseEntity.ok(productService.findById(id));
    }

    @PostMapping
    public ResponseEntity<Product> createProduct(@RequestBody ProductDto productDto) {
        return ResponseEntity.ok(this.productService.createProduct(productDto));
    }

    @Deprecated
    @PutMapping
    public ResponseEntity updateProduct(@RequestBody ProductDto productDto) throws S4sEntityNotFoundException {
        return ResponseEntity.ok(this.productService.updateProduct(dozerMapper.map(productDto, Product.class)));

    }

}
