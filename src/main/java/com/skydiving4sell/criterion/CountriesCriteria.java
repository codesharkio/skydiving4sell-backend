package com.skydiving4sell.criterion;

import lombok.*;

/**
 * Created by hungbang on 05/12/2018
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class CountriesCriteria {
    private String name;
}
