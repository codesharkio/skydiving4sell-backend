package com.skydiving4sell.criterion;

import lombok.*;

/**
 * Created by hungbang on 05/12/2018
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
@Builder
public class CurrenciesCriteria {
    private String code;

}
