package com.skydiving4sell.model;

import lombok.*;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@EqualsAndHashCode(callSuper = false)
public class CustomerReviewDto extends BaseDto {

    private PersonDto person;
    private int score;
    private String title;
    private String detail;
    private List<ImageDto> reviewImages;
    private ProductDto product;


    //** custom code after this line **//
}
