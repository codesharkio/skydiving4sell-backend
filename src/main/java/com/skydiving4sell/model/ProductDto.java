package com.skydiving4sell.model;

import com.skydiving4sell.domain.ProductState;
import io.swagger.annotations.ApiModel;
import lombok.*;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

/**
 * Created by hungbang on 05/12/2018
 * Pojo class to transfer data from backend to client and vice versa
 * XXX:  [HBQ] using the @Mapping("this") to ignore field when dozer mapping
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "Product properties", description = "Product properties")
public class ProductDto extends BaseDto{

    private String title;
    private String titleSummary;
    private String modelNumber;
    private BigDecimal price;
    private String description;
    private String sizeNumber;
    private String color;
    private boolean active;
    private LocalDate listedDate;
    private String domesticShipping;
    private String internationalShipping;
    private ProductState productState;

    //** to be manual mapping because dozer will skip the difference name  **//

    private List<ImageDto> productImages;
    private ManufacturerDto manufacturer;
    private List<CustomerReviewDto> customerReviews;


    //** custom code after this line **//
}
