package com.skydiving4sell.model;

import com.skydiving4sell.domain.Gender;
import lombok.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@EqualsAndHashCode(callSuper = false)
public class PersonDto extends BaseDto {
    private String firstName;
    private String lastName;
    private Gender gender;
}
