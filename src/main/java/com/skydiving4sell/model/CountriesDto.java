package com.skydiving4sell.model;

import com.google.gson.annotations.SerializedName;
import lombok.*;

import java.util.List;

/**
 * Created by hungbang on 05/12/2018
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@EqualsAndHashCode(callSuper = false)
public class CountriesDto extends BaseDto{
    private String name;
    private String alpha2Code;
    private String alpha3Code;
    private String numericCode;
    private String nativeName;

    @SerializedName("currencies")
    private List<CurrencyDto> currencies;
    private String flag;
}
