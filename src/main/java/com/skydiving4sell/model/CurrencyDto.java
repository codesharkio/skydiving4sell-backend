package com.skydiving4sell.model;

import lombok.*;

/**
 * Created by hungbang on 05/12/2018
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@EqualsAndHashCode(callSuper = false)
public class CurrencyDto extends BaseDto{
    private String code;
    private String name;
    private String symbol;
}
