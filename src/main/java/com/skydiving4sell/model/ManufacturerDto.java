package com.skydiving4sell.model;

import lombok.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@EqualsAndHashCode(callSuper = false)
public class ManufacturerDto extends BaseDto {

    private String name;



    //** custom code after this line **//
}
