package com.skydiving4sell.model;


import com.skydiving4sell.domain.ImageType;
import lombok.*;


@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@EqualsAndHashCode(callSuper = false)
public class ImageDto extends BaseDto {

    private ImageType imageType;
    private String url;
    private String data;
    private String name;
    private String extension;
    private long size;
}
