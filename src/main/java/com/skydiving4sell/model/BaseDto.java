package com.skydiving4sell.model;

import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.time.LocalDateTime;

public abstract class BaseDto implements Serializable {

    @ApiModelProperty(hidden = true)
    protected String id;

    @ApiModelProperty(hidden = true)
    protected LocalDateTime creationDate;
    @ApiModelProperty(hidden = true)
    protected LocalDateTime lastUpdatedDate;
    @ApiModelProperty(hidden = true)
    protected String createdBy;
    @ApiModelProperty(hidden = true)
    protected String modifiedBy;

}
