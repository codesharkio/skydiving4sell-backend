package com.skydiving4sell.exception;

/**
 * Created by hungbang on 05/12/2018
 */
public class S4sEntityNotFoundException extends Exception {
    public S4sEntityNotFoundException() {
        super();
    }

    public S4sEntityNotFoundException(String message) {
        super(message);
    }

    public S4sEntityNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }

    public S4sEntityNotFoundException(Throwable cause) {
        super(cause);
    }

    protected S4sEntityNotFoundException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
