package com.skydiving4sell.properties;

import lombok.*;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * Created by hungbang on 11/12/2018
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
@Builder
@Configuration
@ConfigurationProperties("google")
public class GoogleCloudProperties {
    private Application application = new Application();

    @Data(staticConstructor = "of")
    public static class Application{
        private String credentials;
        private String projectId;
    }
}
