package com.skydiving4sell.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * Created by hungbang on 04/12/2018
 */
@Configuration
@ConfigurationProperties("datasource")
public class DataSourceProperties {
    private String url;
    private String serverName;
    private Integer portNumber;
    private String databaseName;
    private Integer poolsize;
    private String className;


    private String username;
    private String password;

    private final Atomikos atomikos = new Atomikos();
    private final Hibernate hibernate = new Hibernate();

    public Atomikos getAtomikos() {
        return atomikos;
    }

    public Hibernate getHibernate() {
        return hibernate;
    }

    public static class Atomikos {
        private String serviceFactory;
        private String logBaseName;
        private Boolean forceShutdownOnVmExit;
        private int transactionTimeout;
        private String testQuery;

        public String getServiceFactory() {
            return serviceFactory;
        }

        public void setServiceFactory(String serviceFactory) {
            this.serviceFactory = serviceFactory;
        }

        public String getLogBaseName() {
            return logBaseName;
        }

        public void setLogBaseName(String logBaseName) {
            this.logBaseName = logBaseName;
        }

        public Boolean getForceShutdownOnVmExit() {
            return forceShutdownOnVmExit;
        }

        public void setForceShutdownOnVmExit(Boolean forceShutdownOnVmExit) {
            this.forceShutdownOnVmExit = forceShutdownOnVmExit;
        }

        public int getTransactionTimeout() {
            return transactionTimeout;
        }

        public void setTransactionTimeout(int transactionTimeout) {
            this.transactionTimeout = transactionTimeout;
        }

        public String getTestQuery() {
            return testQuery;
        }

        public void setTestQuery(String testQuery) {
            this.testQuery = testQuery;
        }
    }

    public static class Hibernate {
        private final Cache cache = new Cache();

        public Cache getCache() {
            return cache;
        }

        public static class Cache {
            private boolean useSecondLevelCache;
            private boolean useQueryCache;

            public boolean isUseSecondLevelCache() {
                return useSecondLevelCache;
            }

            public void setUseSecondLevelCache(boolean useSecondLevelCache) {
                this.useSecondLevelCache = useSecondLevelCache;
            }

            public boolean isUseQueryCache() {
                return useQueryCache;
            }

            public void setUseQueryCache(boolean useQueryCache) {
                this.useQueryCache = useQueryCache;
            }
        }
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getServerName() {
        return serverName;
    }

    public void setServerName(String serverName) {
        this.serverName = serverName;
    }

    public String getDatabaseName() {
        return databaseName;
    }

    public void setDatabaseName(String databaseName) {
        this.databaseName = databaseName;
    }

    public Integer getPortNumber() {
        return portNumber;
    }

    public void setPortNumber(Integer portNumber) {
        this.portNumber = portNumber;
    }

    public Integer getPoolsize() {
        return poolsize;
    }

    public void setPoolsize(Integer poolsize) {
        this.poolsize = poolsize;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }
}
