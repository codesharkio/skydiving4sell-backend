package com.skydiving4sell.utils;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;
import java.time.format.SignStyle;
import java.util.GregorianCalendar;

import static java.time.temporal.ChronoField.*;
import static java.time.temporal.ChronoField.MINUTE_OF_HOUR;

/**
 * Created by hungbang on 04/12/2018
 */
public class DateUtils {
    public static final String DATE_PATTERN = "dd.MM.yyyy";
    public static final String DATE_LONG_PATTERN = "d MMMM yyyy";
    public static final String DATE_LONG_ORDINAL_PATTERN = "d'%s' MMMM yyyy";
    public static final String DATE_TIME_PATTERN = "dd.MM.yyyy HH:mm:ss";
    public static final String TIME_PATTERN = "HH:mm";
    public static final String YEAR_CENTURY_PATTERN = "yy";
    public static final DateTimeFormatter YEAR_CENTURY_FORMATTER = DateTimeFormatter.ofPattern(YEAR_CENTURY_PATTERN);
    /**
     * DateTimeFormatter ofPattern "dd.mm.yyyy"
     */
    public static final DateTimeFormatter DATE_FORMATTER = new DateTimeFormatterBuilder()
            .appendValue(DAY_OF_MONTH, 2)
            .appendLiteral('.')
            .appendValue(MONTH_OF_YEAR, 2)
            .appendLiteral('.')
            .appendValue(YEAR, 4, 10, SignStyle.EXCEEDS_PAD)
            .toFormatter();
    /**
     * DateTimeFormatter ofPattern "dd.mm.yyyy, hh:mm"
     */
    public static final DateTimeFormatter DATE_TIME_FORMATTER = new DateTimeFormatterBuilder()
            .appendValue(DAY_OF_MONTH, 2)
            .appendLiteral('.')
            .appendValue(MONTH_OF_YEAR, 2)
            .appendLiteral('.')
            .appendValue(YEAR, 4, 10, SignStyle.EXCEEDS_PAD)
            .appendLiteral(", ")
            .appendValue(HOUR_OF_DAY, 2)
            .appendLiteral(":")
            .appendValue(MINUTE_OF_HOUR, 2)
            .appendLiteral(":")
            .appendValue(SECOND_OF_MINUTE, 2)
            .toFormatter();
    /**
     * DateTimeFormatter ofPattern "hh:mm"
     */
    public static final DateTimeFormatter TIME_FORMATTER = new DateTimeFormatterBuilder()
            .appendValue(HOUR_OF_DAY, 2)
            .appendLiteral(":")
            .appendValue(MINUTE_OF_HOUR, 2)
            .toFormatter();


    private DateUtils() {
    }


    public static GregorianCalendar toGregorianCalendar(LocalDateTime date) {
        if (date == null) {
            return null;
        }
        return GregorianCalendar.from(date.atZone(ZoneId.systemDefault()));
    }

    public static GregorianCalendar toGregorianCalendar(LocalDate date) {
        if (date == null) {
            return null;
        }
        return GregorianCalendar.from(date.atStartOfDay(ZoneId.systemDefault()));
    }
}
