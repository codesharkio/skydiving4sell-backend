#   To start application :
##  For frontend DevTeam: needs to install `maven` before start
### Mac Os
`brew install maven`

### Ubuntu OS
[How to install maven on ubuntu](https://linuxize.com/post/how-to-install-apache-maven-on-ubuntu-18-04/)
##  Maven build docker images :

`mvn clean install`
##  Deploy application 
`docker-compose up --build -d backend_service`
##  For test: 

http://localhost:8500/ui/dc1/services/skydiving-service

#  Start docker container 
-   MySQL, hazelcast, hazelcast-managent
```
docker run -d --name mysql -p 33306:3306 mysql
docker run -d --name hazelcast -p 5701:5701 hazelcast/hazelcast
docker run -d --name hazelcast-mgmt -p 38080:8080 hazelcast/management-center:3.10
```

If we would like to connect with Hazelcast Management Center from Hazelcast instance we need to place custom `hazelcast.xml` in `/opt/hazelcast` catalog inside Docker container. 
This can be done in two ways, by extending hazelcast base image or just by copying file to existing hazelcast container and restarting it.

## Extending hazelcast base image
```
cd src/main/docker/hazelcast
docker build -t hazelcast:1.0 .
docker run -d --name hazelcast -p 5701:5701 hazelcast:1.0
```

```
docker run -d --name hazelcast -p 5701:5701 hazelcast:1.0
docker stop hazelcast
docker start hazelcast
```

Here’s the most important Hazelcast’s configuration file fragment.

```
<hazelcast xmlns="http://www.hazelcast.com/schema/config" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.hazelcast.com/schema/config http://www.hazelcast.com/schema/config/hazelcast-config-3.8.xsd">
     <group>
          <name>dev</name>
          <password>dev-pass</password>
     </group>
     <management-center enabled="true" update-interval="3">http://192.168.99.100:38080/mancenter</management-center>
...
</hazelcast>
```

- [How to enable L2 cache for JPA and spring boot](https://github.com/hungbang/java-standard/blob/master/how-to-enable-L2-cache-jpa-hazelcast.md)

